class Code
  attr_accessor :pegs

  def initialize(pegs)
    @pegs = pegs
  end

  PEGS = {
    red: "r",
    green: "g",
    blue: "b",
    yellow: "y",
    orange: "o",
    purple: "p"
  }

  def self.parse(pegs)
    pegs.downcase.chars.each do |ch|
      if PEGS.values.include?(ch) == false
        raise ArgumentError
      end
    end

    Code.new(pegs.downcase.chars)
  end

  def self.random
    options = PEGS.values
    first_pos = options.shuffle[0]
    second_pos = options.shuffle[0]
    third_pos = options.shuffle[0]
    fourth_pos = options.shuffle[0]

    Code.new([first_pos, second_pos, third_pos, fourth_pos])
  end

  def [](num)
    pegs[num]
  end

  def exact_matches(code)
    counter = 0
    code.pegs.each_with_index do |val, i|
      pegs.each_with_index do |val2, i2|
        if val == val2 && i == i2
          counter += 1
        end
      end
    end
    counter
  end

  def near_matches(code)
    counter = 0
    answer = pegs
    guess = code.pegs
    guess.each_index do |i|
      if guess[i] == answer[i]
        next
      elsif answer.include?(guess[i])
        counter += 1
        answer.delete(guess[i])
      end
    end
    counter
  end

  def ==(code)
    if code.class != Code
      return false
    end
    pegs == code.pegs
  end
end

class Game
  attr_reader :secret_code
  attr_accessor :guess

  def initialize(secret_code = Code.random)
    @secret_code = secret_code
    @guess = ""
  end

  def get_guess
    Code.parse(guess)
  end

  def display_matches(code)
    puts "exact matches: #{secret_code.exact_matches(code)}"
    puts "near matches: #{secret_code.near_matches(code)}"
  end

  def play
    counter = 0
    puts "welcome to mastermind"
    until counter == 10
      puts "what is your guess?"
      puts "(guess colors: r, g, b, y, o, p)"
      puts "(guess format: gbpr)"
      $stdout.flush
      thing = gets.get_guess
      display_matches(thing)
      if exact_matches(thing) == 4
        puts "congratulations"
        break
      end
      counter += 1
    end
  end
end

if __FILE__ == $PROGRAM_NAME
  t = Game.new
  t.play
end
